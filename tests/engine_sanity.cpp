#include <iostream>

#include "core/math/Vec.hpp"

#include <iostream>
#include <cmath>

#include <fmt/format.h>

#include "core/engine/Engine.hpp"

using namespace engine;

Engine<Sprite, AudioEmitter> eng;

int fun() {

    Engine<Sprite, AudioEmitter> eng;

    std::cout << eng.GetComponentId<Sprite>().id << std::endl;
    std::cout << eng.GetComponentId<AudioEmitter>().id << std::endl;
    // std::cout << eng.GetComponentId<int>() << std::endl;

    Engine<Sprite, AudioEmitter, Sprite> eng2;
    std::cout << eng2.GetComponentId<Sprite>().id << std::endl;
    std::cout << eng2.GetComponentId<AudioEmitter>().id << std::endl;
    // std::cout << eng2.GetComponentId<int>() << std::endl;

    // eng.GetComponent<Sprite>(1);
    eng.GetComponent<Sprite>(engine::Entity());
    return 5;
}

int main() {
    fun();

    return -1;
}
