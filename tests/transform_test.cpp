#include "core/allegro/Transform.hpp"
#include "core/math/Vec.hpp"

#include "core/utils/testing.hpp"

int main() {
    allegro::Transform tm;
    auto vec1 = math::Vec<float, 2>{3, 4 };
    
    auto vec = tm * math::Vec<float, 2>{3, 4 };
    TEST_FUZZY_EQUAL(vec, vec1);
    
    allegro::Transform tr({3, 4}, {2, 2}, 0); // scale -> transl
    vec1 = tr * math::Vec<float, 2>{0, 4 };
    math::Vec<float, 2> vf{3, 12};
    TEST_FUZZY_EQUAL(vf, vec1);
    
    allegro::Transform tr3({3, 4}); // transl
    auto vec3 = tr3 * math::Vec<float, 2>{-1, -1 };
    auto vec4 = math::Vec<float, 2>{2, 3};
    TEST_FUZZY_EQUAL(vec4, vec3);
    
    allegro::Transform tr4({0, 0}, {1, 1}, math::Radian(math::pi)); // rot
    auto vec5 = tr4 * math::Vec<float, 2>{2, 3 };
    auto vec6 = math::Vec<float, 2>{-2, -3};
    TEST_FUZZY_EQUAL(vec5, vec6);
    
    allegro::Transform tr5({3, 4}, {2, 2}, math::Radian(math::pi) / 2); // rot- > scale -> transl
    auto vec7 = tr5 * math::Vec<float, 2>{-2, 4 };
    auto vec8 = math::Vec<float, 2>{-5, 0};
    TEST_FUZZY_EQUAL(vec7, vec8);
    
    allegro::Transform tr6; // rot- > scale -> transl
    tr6.Rotate(math::Radian(math::pi) / 2).Scale({2,2}).Translate({3,4});
    auto vec9 = tr6 * math::Vec<float, 2>{-2, 4 };
    TEST_FUZZY_EQUAL(vec9, vec8);
    
    
    allegro::Transform tr7; // rot- > scale -> transl
    tr7 *= allegro::Transform::Translation({3,4})
        * allegro::Transform::Scaling({2,2}) 
        * allegro::Transform::Rotation(math::Radian(math::pi) / 2);
    allegro::Transform tr8; // rot- > scale -> transl
    tr8 = allegro::Transform::Translation({3,4})
        * allegro::Transform::Scaling({2,2}) 
        * allegro::Transform::Rotation(math::Radian(math::pi) / 2);
    auto vec10 = tr7 * math::Vec<float, 2>{-2, 4 };
    TEST_FUZZY_EQUAL(vec10, vec8);
    auto vec11= tr8 * math::Vec<float, 2>{-2, 4 };
    TEST_FUZZY_EQUAL(vec11, vec8);

    TEST_FUZZY_EQUAL(tr7, tr8);

    return testing::Exit();
}
