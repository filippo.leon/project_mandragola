#include "core/math/Vec.hpp"

#include <iostream>
#include <cmath>

#include <fmt/format.h>

#include "core/allegro/Allegro.hpp"

#include "core/allegro/Display.hpp"
#include "core/allegro/Timer.hpp"
#include "core/allegro/EventQueue.hpp"
#include "core/allegro/Color.hpp"
#include "core/allegro/Font.hpp"
#include "core/allegro/Bitmap.hpp"

#include "core/utils/Timer.hpp"

int main() {
    allegro::Allegro al;

    // allegro::Allegro &inst = allegro::Allegro::Instance();

    allegro::Display display({600, 600});

    allegro::EventQueue &queue = al.CreateAndAttachEventQueue();

    allegro::Timer timer(1s / 60.0f);

    queue.Register(timer);
    queue.Register(display);

    {
        allegro::DisplayBinder db(display);
        db.Clear(allegro::Color(math::Vec<float, 4>{.0f, .4f, .5f, .8f}));
        db.Flip();
    }

    utils::Timer tm;
    utils::MovingAverage<utils::Timer::duration> avg(10);

    auto resources = al.GetPath(allegro::BasePath::Resources).parent_path().parent_path();
    allegro::Font font(resources / "resources/BebasNeue-Regular.ttf", 18);
    allegro::Bitmap image(resources / "resources/spritesheet_new.jpg");
    allegro::FontFamily fam(resources / "resources/BebasNeue-Regular.ttf");

    float t = .1f;
    al.Loop([&, t] () mutable {
        {
            utils::Timer::duration time = tm.Elapsed();
            avg.Push(time);
            
            allegro::DisplayBinder db(display);
            db.Clear(allegro::Color(math::Vec<float, 4>{.0f, (t - std::floor(t)), .5f, .8f}));

            float timeMs = std::chrono::duration_cast<std::chrono::duration<float, std::milli>>(avg.Get()).count();
            font.Draw(fmt::format("{:.0f} FPS ({:.2f} ms)", 1000.0 / timeMs, timeMs), allegro::Color(1.0f,1.0f,1.0f,1.0f), {0, 0});

            fam.Draw(fmt::format("Hello {}!", "<Your Name>"), allegro::Color::green, {40, 40}, 14);

            image.Draw({200, 200});

            db.Flip();

            t += .523f;
            
            tm.Reset();
        }
    });

    return 0;
}
