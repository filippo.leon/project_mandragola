#include <iostream>

#include <fmt/format.h>

#include "core/math/Vec.hpp"
#include "core/math/base.hpp"
#include "core/math/angle.hpp"

#include "core/utils/testing.hpp"
    
int main() {
    math::Vec<int, 2> v{1, 2};
    TEST(v.y == v.j && v.j == 2);
    
    math::Vec<int, 3> w{1, 2, 4};
    TEST_EQUAL(w.z, w.k);
    
    math::Degree deg(60.0f);
    TEST_FUZZY_EQUAL(math::pi / 3, math::Radian(deg).val);
    
    TEST_FUZZY_EQUAL(std::cos(math::pi), -1.0f);
    
    
    // auto w2 = w * 2;
    std::cout << math::Abs(3) << std::endl;
    std::cout << math::Abs(math::Vec<int, 2>{1, -2}) << std::endl;
    std::cout << math::Abs(math::Vec<float, 2>{-1, 2}) << std::endl;
    std::cout << math::Norm(-3) << std::endl;
    std::cout << math::Norm(math::Vec<int, 2>{1, 2}) << std::endl;
    std::cout << math::Norm(math::Vec<float, 2>{1, 2}) << std::endl;
    
    auto zero2 = math::Vec<int, 2>::Zero;
    TEST_FUZZY_EQUAL(v - v, zero2);
    
    TEST_FUZZY_EQUAL(math::Abs(v), v);
    math::Vec<int, 2> v2{-1, 2};
    TEST_FUZZY_EQUAL(math::Abs(v2), v);

    return testing::Exit();
}
