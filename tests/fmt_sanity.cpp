#include <iostream>

#include <fmt/format.h>

int main() {
    std::cout << fmt::format("Hello, World, from fmtlib! {}", 5) << std::endl;
    return 0;
}