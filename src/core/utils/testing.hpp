#pragma once

#include <string>
#include <iostream>

#include <fmt/format.h>
#include <fmt/ostream.h>

#include "core/math/base.hpp"

namespace testing {

inline int failureCount = 0;

inline void TestCondition(bool cond, std::string expr, std::string file, int line) {
    if (!cond) {
        std::cout << fmt::format("Condition '{}' failed in file {}:{}\n", expr, file, line);
        ++failureCount;
    }
}

template <class T>
void TestCondition(bool cond, std::string expr, std::string file, int line, T x, T y) {
    if (!cond) {
        std::cout << fmt::format("Condition '{}' failed in file {}:{} (with left = {}, right = {})\n", expr, file, line, x, y);
        ++failureCount;
    }
}

template <class T>
void TestFuzzyCondition(T x, T y, std::string xs, std::string ys, 
                   std::string file, int line, float tol = 10 * std::numeric_limits<float>::epsilon()) {
    auto val = math::Norm(x - y);
    bool cond = val <= tol;
    if (!cond) {
        std::cout << fmt::format("Condition '{} == {}' failed in file {}:{} (|{} - {}| = {} > {:.2e})\n", xs, ys, file, line, x, y, val, tol);
        ++failureCount;
    }
}

inline int Exit() {
    return failureCount;
}

} // namespace testing

#define TEST(x) \
    { testing::TestCondition(!!(x), #x, __FILE__, __LINE__); }
    
#define TEST_FUZZY_EQUAL(x, y) \
    { testing::TestFuzzyCondition((x), (y), #x, #y, __FILE__, __LINE__); }
    
#define TEST_EQUAL(x, y) \
    TEST_OP(x, y, ==)
    
#define TEST_OP(x, y, op) \
    { testing::TestCondition((!!((x op y))), #x" "#op" "#y, __FILE__, __LINE__, x, y); }
    
