/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

namespace utils::id {

template <typename T, T t_value = T()>
struct InvalidValue {

    static constexpr T value = t_value;
};

template <>
struct InvalidValue<int> {

    static constexpr int value = -1;
};

template <typename T>
class Object {
public:
    constexpr Object() = default;
    constexpr Object(T id) : id(id) {};

    T id = InvalidValue<T>::value;
};


template <typename T, int N>
class FixedObject : public Object<T> {
public:
    constexpr FixedObject() = default;
    constexpr FixedObject(T id) : Object<T>(id) {};

    static constexpr int size = N;
};


} // namespace utils::id

