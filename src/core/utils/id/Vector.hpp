/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

namespace utils::id {

template <class K, typename T>
class Vector : public std::vector<T> {
public:
    using base = std::vector<T>;

    T &operator[](K k) { return static_cast<base&>(*this)[k.id]; }
    const T &operator[](K k) const { return static_cast<const base&>(*this)[k.id]; }
};

template <class K, typename T>
class Array : public std::array<T, K::size> {
public:
    using base = std::array<T, K::size>;

    T &operator[](K k) { return static_cast<base&>(*this)[k.id]; }
    const T &operator[](K k) const { return static_cast<const base&>(*this)[k.id]; }
};

} // namespace utils::id

