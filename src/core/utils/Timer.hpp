/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <chrono>
#include <queue>

namespace utils {

template <class T>
class MovingAverage {
public:
    MovingAverage(int size) : m_size(size) {
        
    }
    
    void Push(const T &val) {
        m_queue.push(val);
        m_average += val;
        if ((int)m_queue.size() >= m_size) {
            m_average -= m_queue.front();
            m_queue.pop();
        }
    }
    
    T Get() {
        return m_average / m_queue.size();
    }
    
private:
    int m_size;
    std::queue<T> m_queue;
    T m_average = T();
};

class Timer {
public:
    using time_point = std::chrono::high_resolution_clock::time_point;
    using duration = std::chrono::high_resolution_clock::duration;
    
    Timer() 
        : m_start(std::chrono::high_resolution_clock::now()) {
        
    }
    
    void Reset() {
        m_start = std::chrono::high_resolution_clock::now();
    }
    
    duration Elapsed() {
        return std::chrono::high_resolution_clock::now() - m_start;
    }
    
private:
    time_point m_start;
};

}

