/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <string>

#include "core/utils/debug.hpp"

namespace allegro {

class InitError : public IException {
public:
    InitError() = default;
    InitError(std::string msg) : m_msg(msg) {}

    const char *what () const throw () {
         return m_msg.c_str();
    }
private:
    std::string m_msg;

};

} // namespace allegro

