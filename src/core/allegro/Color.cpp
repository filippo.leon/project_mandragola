/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "core/allegro/Color.hpp"

#include <allegro5/allegro.h>
#include <allegro5/allegro_color.h>

#include <core/allegro/exceptions.hpp>
#include <core/math/Vec.hpp>

namespace allegro {

Color::Color(float r, float g, float b, float a) {
    m_color = al_map_rgba_f(r, g, b, a);
}

Color::Color(unsigned char r, unsigned char g, unsigned char b, unsigned char a) {
    m_color = al_map_rgba(r, g, b, a);
}

Color::Color(const math::Vec<unsigned char, 4> &vec)
        : Color(vec.r, vec.g, vec.b, vec.a) {

}

Color::Color(const math::Vec<float, 4> &vec)
        : Color(vec.r, vec.g, vec.b, vec.a) {

}

} // namespace allegro