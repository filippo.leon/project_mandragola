/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "core/allegro/Allegro.hpp"

#include <iostream>
#include <filesystem>

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_image.h>

#include <core/utils/debug.hpp>
#include <core/allegro/exceptions.hpp>
#include <core/allegro/EventQueue.hpp>

namespace allegro {

Allegro::Allegro(Config config) {
    ENSURE(s_instance == nullptr);
    s_instance = this;

    if(!al_init()) {
        throw InitError();
    }

    if (config.fontAddon) {
        m_state.fontInit = al_init_font_addon();
        if (!m_state.fontInit) {
            throw InitError("Failed to init font addon...");
        }
    }
    if (config.ttfAddon) {
        m_state.ttfInit = al_init_ttf_addon();
        if (!m_state.ttfInit) {
            throw InitError("Failed to image ttf addon...");
        }
    }
    
    if (config.imageAddon) {
        m_state.imageInit = al_init_image_addon();
        if (!m_state.imageInit) {
            throw InitError("Failed to init image addon...");
        }
    }
}

Allegro::~Allegro() {
    ENSURE(s_instance != nullptr);
    s_instance = nullptr;
}

Allegro &Allegro::Instance() {
    ENSURE(s_instance != nullptr);
    return *s_instance;
}

EventQueue &Allegro::CreateAndAttachEventQueue() {
    if (!m_eventQueue) {
        m_eventQueue = std::make_unique<EventQueue>();
    }
    return *m_eventQueue;
}

void Allegro::Loop(std::function<void()> drawFn) {
    ENSURE(m_eventQueue != nullptr);

    bool running = true;
    float timeoutTime = 0.06f;

    while (running) {
        ALLEGRO_EVENT event;
        ALLEGRO_TIMEOUT timeout;

        al_init_timeout(&timeout, timeoutTime);

        bool get_event = al_wait_for_event_until(m_eventQueue->Get(), &event, &timeout);

        bool redraw = false;
        if (get_event) {
            switch (event.type) {
                case ALLEGRO_EVENT_TIMER:
                    redraw = true;
                    break;
                case ALLEGRO_EVENT_DISPLAY_CLOSE:
                    running = false;
                    break;
                default:
                    std::cout << "Unhandled event: " << event.type << "!" << std::endl;
                    break;
            }
        }

        if (redraw && al_is_event_queue_empty(m_eventQueue->Get())) {
            drawFn();
        }
    }
}

std::filesystem::path Allegro::GetPath(BasePath basePath, std::filesystem::path path) {
    ALLEGRO_PATH *alPath = al_get_standard_path((int)basePath);
    std::filesystem::path base = std::string(al_path_cstr(alPath, ALLEGRO_NATIVE_PATH_SEP));

    return base.append(path.string());
}

} // namespace allegro
