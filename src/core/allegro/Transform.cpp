/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "core/allegro/Transform.hpp"

#include <allegro5/allegro.h>

namespace allegro {

Transform::Transform() {
    al_identity_transform(&m_transf);
}
    
Transform::Transform(math::Vec<float, 2> pos, math::Vec<float, 2> scale, math::Radian theta) {
    al_build_transform(&m_transf, pos.x, pos.y, scale.x, scale.y, theta.val);
}

Transform::Transform(const Transform &other) {
    al_copy_transform(&m_transf, &other.m_transf);
}

Transform &Transform::Invert() {
    al_invert_transform(&m_transf);
    return *this;
}

Transform &Transform::Translate(math::Vec<float, 2> pos) {
    al_translate_transform(&m_transf, pos.x, pos.y);
    return *this;
}
    
Transform &Transform::Scale(math::Vec<float, 2> scale) {
    al_scale_transform(&m_transf, scale.x, scale.y);
    return *this;
}

Transform &Transform::Rotate(math::Radian rot) {
    al_rotate_transform(&m_transf, rot.val);
    return *this;
}

Transform Transform::Translation(math::Vec<float, 2> pos) {
    Transform transf;
    al_translate_transform(&transf.m_transf, pos.x, pos.y);
    return transf;
}
    
Transform Transform::Scaling(math::Vec<float, 2> scale) {
    Transform transf;
    al_scale_transform(&transf.m_transf, scale.x, scale.y);
    return Transform(transf);
}

Transform Transform::Rotation(math::Radian rot) {
    Transform transf;
    al_rotate_transform(&transf.m_transf, rot.val);
    return transf;
}

math::Vec<float, 2> Transform::operator*(math::Vec<float, 2> coord) const {
    al_transform_coordinates(&m_transf, &coord.x, &coord.y);
    return coord;
}

Transform Transform::operator*(const Transform &other) const {
    Transform tr(other);
    al_compose_transform(&tr.m_transf, &m_transf);
    return tr;
}
    
Transform &Transform::operator*=(const Transform &other) {
    ALLEGRO_TRANSFORM tr;
    al_copy_transform(&tr, &other.m_transf);
    al_compose_transform(&tr, &m_transf);
    m_transf = tr;
    return *this;
}

Transform Transform::operator*(float val) const {
    Transform t(*this);
    al_scale_transform(&t.m_transf, val, val);
    return t;
}
    
Transform &Transform::operator*=(float val) {
    al_scale_transform(&m_transf, val, val);
    return *this;
}

Transform Transform::operator/(float val) const {
    return *this / val;
}

Transform &Transform::operator/=(float val) {
    return *this /= val;
}

} // namespace allegro
