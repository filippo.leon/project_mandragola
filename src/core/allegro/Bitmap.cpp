/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "core/allegro/Bitmap.hpp"

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>

#include <fmt/format.h>

#include "core/allegro/exceptions.hpp"

namespace allegro {

Bitmap::Bitmap(std::filesystem::path path) {
    m_bitmap = al_load_bitmap(path.string().c_str());
    if(!m_bitmap) {
        throw InitError(fmt::format("Failed to load '{}'", path.string()));
    }
}


Bitmap::~Bitmap() {
    al_destroy_bitmap(m_bitmap);
}

void Bitmap::Draw(math::Vec<int, 2> size) const {
    al_draw_bitmap(m_bitmap, size.x, size.y, 0);
}

} // namespace allegro
