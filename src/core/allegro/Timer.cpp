/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "core/allegro/Timer.hpp"

#include <allegro5/allegro.h>

#include <core/allegro/exceptions.hpp>

namespace allegro {

Timer::Timer(std::chrono::duration<float> s) {
    m_timer = al_create_timer(s.count());
    if(!m_timer) {
        throw InitError();
    }
    Start();
}


Timer::~Timer() {
    al_destroy_timer(m_timer);
}

void Timer::Start() {
    al_start_timer(m_timer);
}

} // namespace allegro