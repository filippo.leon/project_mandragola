/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <chrono>

#include "core/utils/debug.hpp"

using namespace std::chrono_literals;

struct ALLEGRO_TIMER;

namespace allegro {

class Timer {
public:
    Timer(std::chrono::duration<float> s);
    ~Timer();

    ALLEGRO_TIMER *Get() {
        ENSURE(m_timer != nullptr);
        return m_timer;
    }

    void Start();
private:
    ALLEGRO_TIMER *m_timer = nullptr;
};

} // namespace allegro

