/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <filesystem>
#include <unordered_map>

#include <allegro5/allegro_font.h>

#include "core/math/Vec.hpp"

struct ALLEGRO_FONT;

namespace allegro {

struct Color;

enum class TextAlignment {
    Left = ALLEGRO_ALIGN_LEFT,
    Center = ALLEGRO_ALIGN_CENTER,
    Right = ALLEGRO_ALIGN_CENTER
};

class Font {
public:
    static constexpr int defaultSize = 12;

    explicit Font();

    explicit Font(std::filesystem::path path, int size = defaultSize);

    void Draw(std::string text, const Color &col, math::Vec<int, 2> pos,
        TextAlignment align = TextAlignment::Left) const;

    ALLEGRO_FONT *Get() { return m_font; }
private:
    ALLEGRO_FONT *m_font = nullptr;
};

class FontFamily {
public:
    explicit FontFamily(std::filesystem::path path);

    Font &GetOrMakeFont(int size) const;

    void Draw(std::string text, const Color &col, math::Vec<int, 2> pos, int size = Font::defaultSize,
              TextAlignment align = TextAlignment::Left) const;

private:
    std::filesystem::path m_path;

    mutable std::unordered_map<int, Font> m_fontMap;
};

} // namespace allegro

