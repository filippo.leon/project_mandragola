/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <filesystem>

#include <allegro5/allegro.h>

#include "core/math/Vec.hpp"
#include "core/math/angle.hpp"

namespace allegro {

class Transform {
public:
    Transform();
    Transform(math::Vec<float, 2> pos, math::Vec<float, 2> scale = {1.0f, 1.0f}, math::Radian theta = .0_rad);
    Transform(const Transform &other);
    
    Transform &Invert();
    Transform &Translate(math::Vec<float, 2> pos);
    Transform &Scale(math::Vec<float, 2> scale);
    Transform &Rotate(math::Radian rot);
    
    static Transform Translation(math::Vec<float, 2> pos);
    static Transform Scaling(math::Vec<float, 2> scale);
    static Transform Rotation(math::Radian rot);
    
    math::Vec<float, 2> operator*(math::Vec<float, 2> coord) const;
    
    Transform operator*(const Transform &other) const;
    Transform &operator*=(const Transform &other);
    
    Transform operator*(float val) const;
    Transform &operator*=(float val);
    
    Transform operator/(float val) const;
    Transform &operator/=(float val);
    
    float &operator()(int i, int j) {
        return m_transf.m[i][j];
    }
    const float &operator()(int i, int j) const {
        return m_transf.m[i][j];
    }
private:
    ALLEGRO_TRANSFORM m_transf;
};

inline float Norm(const Transform &a, float p = 2) {
    float ret = .0f;
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            ret += std::pow(math::Norm(a(i, j)), p);
        }
    }
    return std::pow(ret, 1.0f / p);
}

inline Transform Abs(const Transform &a) {
    Transform b;
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            b(i, j) = math::Abs(a(i, j));
        }
    }
    return b;
}

template <class T, unsigned int N>
std::ostream &operator<<(std::ostream &o, const math::Vec<T, N> &a) {
    o << "[\n";
    for (int i = 0; i < 4; ++i) {
        o << "[";
        for (int j = 0; j < 4; ++j) {
            o << a[i][j] << ",";
        }
        o << "]\n";
    }
    o << "]";
    return o;
}

} // namespace allegro

