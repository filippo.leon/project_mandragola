/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "core/allegro/EventQueue.hpp"

#include <allegro5/allegro.h>

#include <core/allegro/exceptions.hpp>

#include "core/allegro/Display.hpp"
#include "core/allegro/Timer.hpp"

namespace allegro {

EventQueue::EventQueue() {
    m_eventQueue = al_create_event_queue();
    if(!m_eventQueue) {
        throw InitError();
    }
}


EventQueue::~EventQueue() {
    al_destroy_event_queue(m_eventQueue);
}

void EventQueue::Register(Display &display) {
    al_register_event_source(m_eventQueue, al_get_display_event_source(display.Get()));
}

void EventQueue::Register(Timer &timer) {
    al_register_event_source(m_eventQueue, al_get_timer_event_source(timer.Get()));
}

} // namespace allegro