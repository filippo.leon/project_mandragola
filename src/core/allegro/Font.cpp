/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "core/allegro/Font.hpp"

#include <filesystem>

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

#include <fmt/format.h>

#include <core/allegro/exceptions.hpp>
#include <core/allegro/Color.hpp>

namespace allegro {

Font::Font() {}

Font::Font(std::filesystem::path path, int size) {
    m_font = al_load_ttf_font(path.string().c_str(), size, 0);
    if (!m_font) {
        throw InitError(fmt::format("Unable to load font {}", path.string()));
    }
}

void Font::Draw(std::string text, const Color &col, math::Vec<int, 2> pos,
                TextAlignment align) const {
    al_draw_text(m_font, col.Get(), pos.x, pos.y, (int)align, text.c_str());

}

FontFamily::FontFamily(std::filesystem::path path) : m_path(path) {
    GetOrMakeFont(Font::defaultSize);
}

Font &FontFamily::GetOrMakeFont(int size) const {
    auto it = m_fontMap.find(size);
    if (it == m_fontMap.end()) {
        return m_fontMap[size] = Font(m_path, size);

    }
    return it->second;
}

void FontFamily::Draw(std::string text, const Color &col, math::Vec<int, 2> pos, int size,
          TextAlignment align) const {
    al_draw_text(GetOrMakeFont(size).Get(), col.Get(), pos.x, pos.y, (int)align, text.c_str());

}

} // namespace allegro
