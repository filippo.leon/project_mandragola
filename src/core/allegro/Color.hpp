/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <allegro5/allegro.h>

#include "core/math/Vec.hpp"

namespace allegro {

class Color {
public:
    explicit Color(float r, float b, float g, float a = 1.0f);
    explicit Color(unsigned char r, unsigned char b, unsigned char g, unsigned char a = 255);
    explicit Color(int r, int b, int g, int a = 255)
            : Color((unsigned char) r, (unsigned char) g, (unsigned char) b, (unsigned char) a) {}
    explicit Color(const math::Vec<unsigned char, 4> &vec);
    explicit Color(const math::Vec<float, 4> &vec);
    Color(const ALLEGRO_COLOR& col) : m_color(col) {

    }

    template <class T, typename std::enable_if_t<std::is_same_v<unsigned char, T>>* = nullptr>
    math::Vec<unsigned char, 4> GetAs() const {
        math::Vec<unsigned char, 4> vec;
        al_unmap_rgba(m_color, &vec.r, &vec.g, &vec.b, &vec.a);
        return vec;
    }

    template <class T, typename std::enable_if_t<std::is_same_v<float, T>>* = nullptr>
    math::Vec<float, 4> GetAs() const {
        math::Vec<float, 4> vec;
        al_unmap_rgba_f(m_color, &vec.r, &vec.g, &vec.b, &vec.a);
        return vec;
    }

    ALLEGRO_COLOR Get() const {
        return m_color;
    }

    static constexpr ALLEGRO_COLOR white {1.0f, 1.0f, 1.0f, 1.0f};
    static constexpr ALLEGRO_COLOR black {0.0f, 0.0f, 0.0f, 1.0f};
    static constexpr ALLEGRO_COLOR red   {1.0f, 0.0f, 0.0f, 1.0f};
    static constexpr ALLEGRO_COLOR green {0.0f, 1.0f, 0.0f, 1.0f};
    static constexpr ALLEGRO_COLOR blue  {0.0f, 0.0f, 1.0f, 1.0f};
private:
    ALLEGRO_COLOR m_color;
};

} // namespace allegro

