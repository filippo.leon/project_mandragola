/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "core/math/Vec.hpp"

struct ALLEGRO_DISPLAY;

namespace allegro {

class Color;

class Display {
public:
    Display(math::Vec<int, 2> v);
    ~Display();

    void Flip();

    ALLEGRO_DISPLAY *Get() {
        return m_display;
    }
private:
    ALLEGRO_DISPLAY *m_display = nullptr;
};

class DisplayBinder {
public:
    DisplayBinder(Display &disp);
    ~DisplayBinder();

    void Flip();
    void Clear(const Color &color);
};

} // namespace allegro

