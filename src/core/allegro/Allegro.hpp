/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <functional>
#include <memory>
#include <filesystem>

#include <allegro5/allegro.h>

namespace allegro {

class EventQueue;

enum class BasePath {
    Resources = ALLEGRO_RESOURCES_PATH,
    UserHome = ALLEGRO_USER_HOME_PATH,
    UserData = ALLEGRO_USER_DATA_PATH,
    Settings = ALLEGRO_USER_SETTINGS_PATH,
    Temp = ALLEGRO_TEMP_PATH,
};

struct Config {
    bool fontAddon = true;
    bool ttfAddon = true;
    bool primitivesAddon = true;
    bool imageAddon = true;

};

struct State {
    bool fontInit = false;
    bool ttfInit = false;
    bool imageInit = false;
};

class Allegro {
public:
    Allegro(Config conf = Config());
    ~Allegro();

    static Allegro &Instance();

    EventQueue &CreateAndAttachEventQueue();

    void Loop(std::function<void()>);

    std::filesystem::path GetPath(BasePath basePath, std::filesystem::path path = "");
private:
    Allegro(const Allegro&) = delete;

    inline static Allegro *s_instance = nullptr;

    std::unique_ptr<EventQueue> m_eventQueue;

    Config m_config;
    State m_state;
};

} // namespace allegro

