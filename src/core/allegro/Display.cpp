/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "core/allegro/Display.hpp"

#include <allegro5/allegro.h>

#include <core/allegro/exceptions.hpp>

#include "core/allegro/Color.hpp"

namespace allegro {

Display::Display(math::Vec<int, 2> size) {
    m_display = al_create_display(size.x, size.y);
    if(!m_display) {
        throw InitError("Failed to create display");
    }
}


Display::~Display() {
    al_destroy_display(m_display);
}

void Display::Flip() {
    al_flip_display();
}

DisplayBinder::DisplayBinder(Display &disp) {
    al_set_target_backbuffer(disp.Get());
}

DisplayBinder::~DisplayBinder() {

}

void DisplayBinder::Flip() {
    al_flip_display();
}

void DisplayBinder::Clear(const Color &color) {
    al_clear_to_color(color.Get());
}

} // namespace allegro
