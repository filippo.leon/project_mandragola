/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <cmath>
#include <array>
#include <algorithm>
#include <ostream>

#include "core/math/base.hpp"

namespace math {

template <typename T, unsigned int I>
struct Vec : public std::array<T, I> {
    
    static constexpr Vec<T, I> Zero = {{0}};
};

template <typename T>
struct Vec<T, 1> {
    union { T x;  T i; T r; };
    
    const T &operator[](int i) const {
        return const_cast<const T&>(const_cast<Vec<T, 1>&>(*this)[i]);
    };
    
    T &operator[](int) {
        return x;
    };
    
    static constexpr Vec<T, 2> Zero = Vec<T, 2>();
};

template <typename T>
struct Vec<T, 2> : public Vec<T, 1> {
    union { T y;  T j; T b; };
    
    const T &operator[](int i) const {
        return const_cast<const T&>(const_cast<Vec<T, 2>&>(*this)[i]);
    };
    
    T &operator[](int i) {
        return i == 1 ? y : static_cast<Vec<T, 1>&>(*this)[i];
    };
    
    static constexpr Vec<T, 2> Zero = {0, 0};
};

template <typename T>
struct Vec<T, 3> : public Vec<T, 2> {
    union { T z;  T k; T g; };
    
    const T &operator[](int i) const {
        return const_cast<const T&>(const_cast<Vec<T, 3>&>(*this)[i]);
    };
    
    T &operator[](int i) {
        return i == 2 ? z : static_cast<Vec<T, 2>&>(*this)[i];
    };
    
    static constexpr Vec<T, 2> Zero = {0, 0, 0};
};

template <typename T>
struct Vec<T, 4> : public Vec<T, 3> {
    union { T w;  T l; T a; };
    
    const T &operator[](int i) const {
        return const_cast<const T&>(const_cast<Vec<T, 4>&>(*this)[i]);
    };
    
    T &operator[](int i) {
        return i == 3 ? w : static_cast<Vec<T, 3>&>(*this)[i];
    };
    
    static constexpr Vec<T, 2> Zero = {0, 0, 0, 0};
};

template <class T, unsigned int N, int P = -1>
T Norm(const math::Vec<T, N> &a, T p = 2) {
    T pw = P == -1 ? p : P;
    T c = T();
    for (int i = 0; i < (int)N; ++i) {
        c += std::pow(math::Norm(a[i]), pw);
    }
    return std::pow(c, 1 / pw);
}

template <class T, unsigned int N>
math::Vec<T, N> Abs(const math::Vec<T, N> &a) {
    math::Vec<T, N> c;
    for (int i = 0; i < (int)N; ++i) {
        c[i] = math::Abs(a[i]);
    }
    return c;
}

} // namespace math

template <class T, unsigned int N>
std::ostream &operator<<(std::ostream &o, const math::Vec<T, N> &a) {
    o << "[";
    for (int i = 0; i < (int)N; ++i) {
        o << a[i] << ",";
    }
    o << "]";
    return o;
}

template <class T, unsigned int N>
math::Vec<T, N> operator-(const math::Vec<T, N> &a, const math::Vec<T, N> &b) {
    math::Vec<T, N> c;
    for (int i = 0; i < (int)N; ++i) {
        c[i] = a[i] - b[i];
    }
    return c;
}
