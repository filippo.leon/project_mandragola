/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <vector>
#include <tuple>
#include <type_traits>

#include "core/utils/debug.hpp"

#include "core/utils/id/Object.hpp"
#include "core/utils/id/Vector.hpp"

namespace engine {

struct Sprite {};
struct AudioEmitter {};

class ComponentIndex : public utils::id::Object<int> {

};

class Entity : public utils::id::Object<int> {

};

template <class T>
class ComponentList {
public:
    utils::id::Vector<ComponentIndex, T> components;
};

namespace detail {

template<class S, int size>
constexpr int GetTupleIndexImpl() {
    static_assert(!std::is_same_v<S, S>, "Invalid component 'S'!");
    return -1;
}

template<class S, int size, class T, class ...Args>
constexpr int GetTupleIndexImpl() {
    if constexpr (std::is_same_v<S, T>) {
        return size - sizeof...(Args) - 1;
    } else {
        return GetTupleIndexImpl<S, size, Args...>();
    }
}

} // namespace detail

template <class ...Args>
struct ComponentListHolder : public std::tuple<ComponentList<Args>...> {
    template <unsigned int N>
    auto GetComponentList() const {
        return std::get<N>(*this);
    }

    template <unsigned int N>
    auto GetComponentList() {
        return std::get<N>(*this);
    }

    static constexpr int size = sizeof...(Args);
};

template <class ...Args>
class Engine {
public:
    class ComponentId : public utils::id::FixedObject<int, sizeof...(Args)> {
        using utils::id::FixedObject<int, sizeof...(Args)>::FixedObject;
    };

    template <class S>
    static constexpr ComponentId GetId() {
        return ComponentId(detail::GetTupleIndexImpl<S, sizeof...(Args), Args...>());
    }

    utils::id::Vector<Entity, utils::id::Array<ComponentId, ComponentIndex>> entity2componentIndices;

    ComponentListHolder<Args...> componentListHolder;

    template <class T>
    static constexpr ComponentId GetComponentId() {
        return GetId<T>();
    }

    template <class T>
    ComponentIndex GetComponentIndex(Entity e) const {
        return entity2componentIndices[e][GetComponentId<T>()];
    }

    ComponentIndex GetComponentIndex(Entity e, ComponentId id) const {
        return entity2componentIndices[e][id];
    }

    template <class T>
    T &GetComponent(Entity e) {
        return const_cast<T &>(static_cast<const Engine<Args...> &>(*this).GetComponent<T>(e));
    }

    template <class T>
    const T &GetComponent(Entity e) const {
        constexpr ComponentId id = GetComponentId<T>();
        ComponentIndex index = GetComponentIndex<T>(e);
        const auto &components = componentListHolder.template GetComponentList<id.id>().components;
        ENSURE(index.id >= 0 && index.id < (int)components.size());
        return components[index];
    }
};

} // namespace engine

